//
//  UIViewController+INONavigationController.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INONavigationController.h"

@interface UIViewController (INONavigationController)

@property(nullable, nonatomic,readonly,strong) INONavigationController *navigationController;

@end
