//
//  UIImage+Crop.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "UIImage+Addition.h"

@implementation UIImage (Addition)

+ (UIImage *)imageFromView:(UIView *)view drawedInRect:(CGRect)rect afterScreenUpdates:(BOOL)afterScreenUpdates {
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, [UIScreen mainScreen].scale);
    [view drawViewHierarchyInRect:rect afterScreenUpdates:afterScreenUpdates];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)cropFromRect:(CGRect)fromRect {
    fromRect = CGRectMake(fromRect.origin.x * self.scale,
                          fromRect.origin.y * self.scale,
                          fromRect.size.width * self.scale,
                          fromRect.size.height * self.scale);
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, fromRect);
    UIImage * crop = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return crop;
}


@end
