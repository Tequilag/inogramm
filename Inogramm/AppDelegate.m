//
//  AppDelegate.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "INONavigationController.h"

NSString * const LocalNotificationCategoryIndetifier = @"LocalNotificationCategoryIndetifier";

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [[UINavigationBar appearance] setTintColor:[UIColor hw_colorWithHexInt:INOCrusoeColorHex]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor hw_colorWithHexInt:INODeYorkColorHex]];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [[INONavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Inogramm"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                _persistentContainer.viewContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
