//
//  InstagrammService.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "InstagrammService.h"
#import "Session.h"
#import "Reachability.h"
#import <UIKit/UIKit.h>

NSString *InstagrammServiceReachibilityChangedNotification = @"InstagrammServiceReachibilityChangedNotification";

@interface InstagrammService ()

@property (nonatomic, strong) NSURLSession *instagramDataSession;
@property (nonatomic, strong) NSURLSession *imagesDownloadSession;

@property (nonatomic, strong) Session *session;

@property (nonatomic, strong) Reachability *reachability;

@property (nonatomic, assign) NSInteger requestsCount;

@end

@implementation InstagrammService

+ (instancetype)sharedService {
    static InstagrammService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[InstagrammService alloc] init];
    });
    return sharedService;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _requestsCount = 0;
        
        _session = [[Session alloc] init];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _instagramDataSession = [NSURLSession sessionWithConfiguration:configuration];
        
        _reachability = [Reachability reachabilityForInternetConnection];
        [_reachability startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChangedNotification:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
    }
    return self;
}

- (void)loginWithToken:(NSString *)token {
    self.session.token = token;
}

- (BOOL)isLoggedIn {
    return self.session.token.length > 0;
}

- (void)logoutWithCompletion:(InstagrammServiceCompletion)completion {
    self.session.token = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        NSError *errors;
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        if (completion) {
            completion(YES, nil);
        }
    });
}

- (void)loadMediaWithCompletion:(InstagrammServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/media/recent/?access_token=%@", self.session.token]];
    [self performRequestWithURL:url completion:completion];
}

- (void)loadCommentsForMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/media/%@/comments?access_token=%@", mediaId, self.session.token]];
    [self performRequestWithURL:url completion:completion];
}

- (void)deleteComment:(NSString *)commentId fromMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v1/media/%@/comments/%@?access_token=%@", mediaId, commentId, self.session.token]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"DELETE"];

    [self performRequest:request withCompletion:^(BOOL success, NSData *data, NSError *error) {
        if (completion) {
            completion(success, error);
        }
    }];
}

- (void)createCommentWithText:(NSString *)text forMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceDataCompletion)completion {
    NSString *createCommentURL = [NSString stringWithFormat:@"https://api.instagram.com/v1/media/%@/comments", mediaId];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:createCommentURL]];
    [request setHTTPMethod:@"POST"];
    
    NSString *formBoundary = [NSString stringWithFormat:@"%@.boundary.%08x%08x", @"asdasdasdasd", arc4random(), arc4random()];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", formBoundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"access_token\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"text/plain"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[self.session.token dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"text\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"text/plain"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[text dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    [self performRequest:request withCompletion:completion];
}

- (void)performRequestWithURLString:(NSString *)URLString completion:(InstagrammServiceDataCompletion)completion {
    [self performRequestWithURL:[NSURL URLWithString:URLString] completion:completion];
}

- (void)performRequestWithURL:(NSURL *)URL completion:(InstagrammServiceDataCompletion)completion {
    [self performRequest:[[NSURLRequest alloc] initWithURL:URL] withCompletion:completion];
}

- (void)performRequest:(NSURLRequest *)request withCompletion:(InstagrammServiceDataCompletion)completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.requestsCount++;
    });
    
    [[self.instagramDataSession dataTaskWithRequest:request
                                 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         if (completion) {
                                             completion(!error, data, error);
                                         }
                                         self.requestsCount--;
                                     });
                                 }] resume];
}

- (BOOL)online {
    return [self.reachability currentReachabilityStatus] != NotReachable;
}

- (void)setRequestsCount:(NSInteger)requestsCount {
    _requestsCount = requestsCount;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:_requestsCount > 0];
}

#pragma mark - Private methods

- (void)reachabilityChangedNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] postNotificationName:InstagrammServiceReachibilityChangedNotification object:nil];
}

@end
