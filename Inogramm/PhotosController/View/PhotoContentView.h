//
//  PhotoContentView.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;

@interface PhotoContentView : UIView

@property (nonatomic, assign) UIEdgeInsets contentInsets;

+ (CGFloat)heightForItem:(id)item widht:(CGFloat)widht;

- (void)setItem:(Photo *)item;

- (void)prepareForReuse;

@end
