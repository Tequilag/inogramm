//
//  PhotosModel.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotosModel.h"
#import "AppDelegate.h"

#import "Photo.h"
#import "Pagination.h"

#import "Mapper.h"
#import "InstagrammService.h"

@interface PhotosModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSArray *fetchedPhotos;

@property (nonatomic, strong) Pagination *pagination;

@end

@implementation PhotosModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (NSInteger)numberOfRows {
    return self.fetchedPhotos.count;
}

- (Photo *)itemForRow:(NSInteger)row {
    return [self.fetchedPhotos objectAtIndex:row];
}

- (void)reloadDataWithCompletion:(void (^)())completion {
    if ([[InstagrammService sharedService] online]) {
        [[InstagrammService sharedService] loadMediaWithCompletion:^(BOOL success, NSData *data, NSError *error) {
            if (success) {
                NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                
                [context performBlock:^{
                    Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                        withJSONData:data
                                                             keypath:@"pagination"].firstObject;
                    
                    NSArray *photos = [Mapper entitiesOfClass:[Photo class]
                                                 withJSONData:data
                                                      keypath:@"data"
                                                    inContext:context];
                    
                    [self removeDeletedPhotosWithLoadedPhotos:photos inContext:context];
                    
                    NSError *error = nil;
                    [context save:&error];
                    if (error) {
                        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.pagination = pagination;
                        [self refetchData];
                        
                        if (completion) {
                            completion();
                        }
                    });
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refetchData];
                    if (completion) {
                        completion();
                    }
                });
            }
        }];
    }
    else {
        self.pagination = nil;
        [self refetchData];
        if (completion) {
            completion();
        }
    }
}

- (BOOL)canLoadNextPart {
    return self.pagination && self.pagination.nextURL.length > 0;
}

- (void)loadNextPartWithCompletion:(void (^)())completion {
    if ([[InstagrammService sharedService] online]) {
        if ([self canLoadNextPart]) {
            [[InstagrammService sharedService] performRequestWithURLString:self.pagination.nextURL completion:^(BOOL success, NSData *data, NSError *error) {
                if (success) {
                    NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                    
                    [context performBlock:^{
                        Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                            withJSONData:data
                                                                 keypath:@"pagination"].firstObject;
                        
                        NSArray *photos = [Mapper entitiesOfClass:[Photo class]
                                                     withJSONData:data
                                                          keypath:@"data"
                                                        inContext:context];
                        
                        [self removeDeletedPhotosWithLoadedPhotos:photos inContext:context];
                        
                        NSError *error = nil;
                        [context save:&error];
                        if (error) {
                            NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.pagination = pagination;
                            [self refetchData];
                            
                            if (completion) {
                                completion();
                            }
                        });
                    }];
                }
            }];
        }
        else if (completion) {
                completion();
        }
    }
    else {
        self.pagination = nil;
        [self refetchData];
        if (completion) {
            completion();
        }
    }
}

#pragma mark - Private methods

- (void)refetchData {
    NSFetchRequest *photosRequest = [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
    photosRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"photoId" ascending:NO]];
    
    if ([[InstagrammService sharedService] online]) {
        if (!self.pagination) {
            return;
        }
        
        if (self.pagination.nextPhotoId) {
            photosRequest.predicate = [NSPredicate predicateWithFormat:@"photoId >= %@", self.pagination.nextPhotoId];
        }
    }
    
    NSError *error = nil;
    self.fetchedPhotos = [self.persistentContainer.viewContext executeFetchRequest:photosRequest
                                                                             error:&error];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
    }

}

- (void)removeDeletedPhotosWithLoadedPhotos:(NSArray *)loadedPhotos inContext:(NSManagedObjectContext *)context {
    NSNumber *maxId = [loadedPhotos valueForKeyPath:@"@max.photoId"];
    NSNumber *minId = [loadedPhotos valueForKeyPath:@"@min.photoId"];
    
    NSPredicate *deletedPredicate = [NSPredicate predicateWithFormat:@"(photoId >= %@) AND (photoId <= %@) AND NOT (photoId  IN %@)", minId, maxId, [loadedPhotos valueForKey:@"photoId"]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
    fetchRequest.predicate = deletedPredicate;
    
    NSError *error = nil;
    NSArray *deletedPhotos = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
    }
    
    for (NSManagedObject *entity in deletedPhotos) {
        [context deleteObject:entity];
    }
}

@end
