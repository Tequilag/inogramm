//
//  ShiftAnimator.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "INOAnimator.h"

@interface ShiftAnimator : NSObject <INOAnimator>

+ (instancetype)leftToRightAnimator;
+ (instancetype)rightToLeftAnimator;

@end
