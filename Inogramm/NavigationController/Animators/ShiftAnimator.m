//
//  ShiftAnimator.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ShiftAnimator.h"

@interface LeftToRightShiftAnimator : ShiftAnimator

@end

@interface RightToLeftShiftAnimator : ShiftAnimator

@end

@implementation ShiftAnimator

+ (instancetype)leftToRightAnimator {
    return [[LeftToRightShiftAnimator alloc] init];
}

+ (instancetype)rightToLeftAnimator {
    return [[RightToLeftShiftAnimator alloc] init];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext { return 0.0f; }
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {}

@end

@implementation LeftToRightShiftAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIView *container = [transitionContext containerView];
    
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    toView.frame = container.bounds;
    
    CGAffineTransform defaultTransform = CGAffineTransformMakeTranslation(-container.bounds.size.width, 0.0f);
    toView.transform = defaultTransform;
    [container addSubview:toView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0.0f
                        options:0
                     animations:^{
                         toView.transform = CGAffineTransformIdentity;
                         fromView.transform = CGAffineTransformInvert(defaultTransform);
                     } completion:^(BOOL finished) {
                         [fromView removeFromSuperview];
                         fromView.transform = CGAffineTransformIdentity;
                         [transitionContext completeTransition:finished];
                     }];
}

@end

@implementation RightToLeftShiftAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIView *container = [transitionContext containerView];
    
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    toView.frame = container.bounds;
    
    CGAffineTransform defaultTransform = CGAffineTransformMakeTranslation(container.bounds.size.width, 0.0f);
    toView.transform = defaultTransform;
    [container addSubview:toView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0.0f
                        options:0
                     animations:^{
                         toView.transform = CGAffineTransformIdentity;
                         fromView.transform = CGAffineTransformInvert(defaultTransform);
                     } completion:^(BOOL finished) {
                         [fromView removeFromSuperview];
                         fromView.transform = CGAffineTransformIdentity;
                         [transitionContext completeTransition:finished];
                     }];
}

@end
