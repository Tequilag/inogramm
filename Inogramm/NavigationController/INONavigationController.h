//
//  BackroundNavigationController.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INOAnimator.h"

@interface INONavigationController : UINavigationController

- (void)setDelegate:(id<UINavigationControllerDelegate>)delegate NS_UNAVAILABLE;

- (void)setAnimator:(id <INOAnimator>)animator forTransitionFrom:(Class)firstClass to:(Class)secondClass;

@end

@interface INONavigationController (UITableViewTransition)

- (void)setIndexPath:(NSIndexPath *)indexPath forTransitionBetween:(Class)firstClass and:(Class)secondClass;

@end
