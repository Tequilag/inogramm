//
//  BackroundNavigationController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "INONavigationController.h"
#import "INOTableViewAnimator.h"
#import "ShiftAnimator.h"

static NSString *const INONavigationControllerAnimatorKeyPrefix = @"INONavigationControllerAnimatorKeyPrefix";

@interface INONavigationController () <UINavigationControllerDelegate>

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, strong) NSMutableDictionary <NSString *, id<INOAnimator>> *animators;

@property (nonatomic, strong) id<INOAnimator> defaultPushAnimator;
@property (nonatomic, strong) id<INOAnimator> defaultPopAnimator;

@end

@implementation INONavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        _animators = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super setDelegate:self];
    
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    [self.view insertSubview:self.backgroundImageView atIndex:0];
    
    self.defaultPopAnimator = [ShiftAnimator leftToRightAnimator];
    self.defaultPushAnimator = [ShiftAnimator rightToLeftAnimator];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.backgroundImageView.frame = self.view.bounds;
}

#pragma mark - Public methods

- (void)setAnimator:(id<INOAnimator>)animator forTransitionFrom:(__unsafe_unretained Class)firstClass to:(__unsafe_unretained Class)secondClass {
    NSString *animatorKey = [self animatorKeyFirsClass:firstClass secondClass:secondClass];
    [self.animators setObject:animator forKey:animatorKey];
}

- (void)setIndexPath:(NSIndexPath *)indexPath forTransitionBetween:(Class)firstClass and:(Class)secondClass {
    NSString *firstKey = [self animatorKeyFirsClass:firstClass secondClass:secondClass];
    NSString *secondKey = [self animatorKeyFirsClass:secondClass secondClass:firstClass];
    
    id <INOAnimator> animator = [self.animators objectForKey:firstKey];
    if (animator && [animator conformsToProtocol:@protocol(INOTableViewAnimator)]) {
        [((id <INOTableViewAnimator>)animator) setIndexPath:indexPath];
    }
    
    animator = [self.animators objectForKey:secondKey];
    if (animator && [animator conformsToProtocol:@protocol(INOTableViewAnimator)]) {
        [((id <INOTableViewAnimator>)animator) setIndexPath:indexPath];
    }
}

#pragma mark - Private mthods 

- (NSString *)animatorKeyFirsClass:(Class)firstClass secondClass:(Class)secondClass {
    return [NSString stringWithFormat:@"%@_%@_%@", INONavigationControllerAnimatorKeyPrefix, NSStringFromClass(firstClass), NSStringFromClass(secondClass)];
}

#pragma mark - UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    id <INOAnimator> animator = [self.animators objectForKey:[self animatorKeyFirsClass:[fromVC class] secondClass:[toVC class]]];
    
    if (!animator) {
        if (operation == UINavigationControllerOperationPush) {
            animator = self.defaultPushAnimator;
        }
        else {
            animator = self.defaultPopAnimator;
        }
    }
    
    return animator;
}

@end
