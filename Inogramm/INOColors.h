//
//  INOColors.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#ifndef INOColors_h
#define INOColors_h

//colors http://paletton.com/#uid=12V0u0kllllaFw0g0qFqFg0w0aF
//background http://dragdropsite.github.io/waterpipe.js/

static NSInteger const INOJewelColorHex = 0x105e28;
static NSInteger const INOCrusoeColorHex = 0x004013;
static NSInteger const INODeYorkColorHex = 0x7ebd91;

#endif /* INOColors_h */
