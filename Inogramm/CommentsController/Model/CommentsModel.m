//
//  CommentsModel.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 16/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CommentsModel.h"

#import "Photo.h"
#import "Comment.h"
#import "Pagination.h"

#import "InstagrammService.h"

#import "AppDelegate.h"
#import "Mapper.h"

@interface CommentsModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

@property (nonatomic, strong) NSArray<NSSortDescriptor *> *commentsSortDescriptors;
@property (nonatomic, strong) NSArray<Comment *> *fetchedComments;

@end

@implementation CommentsModel

- (instancetype)initWithPhoto:(Photo *)photo {
    self = [super init];
    if (self) {
        _photo = photo;
        
        _commentsSortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO],  [NSSortDescriptor sortDescriptorWithKey:@"commentId" ascending:NO]];
        _fetchedComments = [_photo.comments sortedArrayUsingDescriptors:_commentsSortDescriptors];
        _persistentContainer = ((AppDelegate *)[UIApplication sharedApplication].delegate).persistentContainer;
        
        [self refetchData];
    }
    return self;
}

- (NSInteger)numberOfRows {
    return self.fetchedComments.count;
}

- (Comment *)itemForRow:(NSInteger)row {
    return self.fetchedComments[row];
}

- (void)reloadDataWithCompletion:(void (^)())completion {
    if ([[InstagrammService sharedService] online]) {
        [[InstagrammService sharedService] loadCommentsForMediaId:self.photo.photoIdString withCompletion:^(BOOL success, NSData *data, NSError *error) {
            if (success) {
                NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                
                [context performBlock:^{
                    NSArray *comments = [Mapper entitiesOfClass:[Comment class]
                                                   withJSONData:data
                                                        keypath:@"data"
                                                      inContext:context];
                    
                    Photo *photo = [context objectWithID:self.photo.objectID];
                    [photo setComments:[NSSet setWithArray:comments]];
                    
                    NSError *error = nil;
                    [context save:&error];
                    if (error) {
                        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self refetchData];
                        
                        if (completion) {
                            completion();
                        }
                    });
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self refetchData];
                    if (completion) {
                        completion();
                    }
                });
            }
        }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self refetchData];
            if (completion) {
                completion();
            }
        });
    }
}

- (void)deleteItemForRow:(NSInteger)row withCompletion:(void (^)(BOOL))completion {
    if ([[InstagrammService sharedService] online]) {
        [[InstagrammService sharedService] deleteComment:[self itemForRow:row].commentId
                                             fromMediaId:self.photo.photoIdString
                                          withCompletion:^(BOOL success, NSError *error) {
                                               [self.persistentContainer.viewContext performBlock:^{
                                                   if (success) {
                                                       Comment *comment = [self itemForRow:row];
                                                       [self.persistentContainer.viewContext deleteObject:comment];
                                                       
                                                       NSError *error = nil;
                                                       [self.persistentContainer.viewContext save:&error];
                                                       [self refetchData];
                                                   }
                                                   if (completion) {
                                                       completion(success);
                                                   }
                                               }];
                                          }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(NO);
            }
        });
    }
}

- (void)createItemWithText:(NSString *)text withCompletion:(void(^)(BOOL created))completion {
    if ([[InstagrammService sharedService] online]) {
        [[InstagrammService sharedService] createCommentWithText:text
                                                      forMediaId:self.photo.photoIdString
                                                  withCompletion:^(BOOL success, NSData *data, NSError *error) {
                                                      [self.persistentContainer.viewContext performBlock:^{
                                                          if (success) {
                                                              Comment *comment = [Mapper entitiesOfClass:[Comment class]
                                                                                            withJSONData:data
                                                                                                 keypath:@"data"
                                                                                               inContext:self.persistentContainer.viewContext].firstObject;
                                                              comment.photos = self.photo;
                                                              
                                                              NSError *error = nil;
                                                              [self.persistentContainer.viewContext save:&error];
                                                              [self refetchData];
                                                          }
                                                          if (completion) {
                                                              completion(success);
                                                          }
                                                      }];

                                                  }];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(NO);
            }
        });
    }
}

#pragma mark - Private methods

- (void)refetchData {
    [self.persistentContainer.viewContext refreshObject:self.photo mergeChanges:NO];
    self.fetchedComments = [self.photo.comments sortedArrayUsingDescriptors:self.commentsSortDescriptors];
}

@end
