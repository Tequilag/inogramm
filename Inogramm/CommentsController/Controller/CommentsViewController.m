//
//  CommentsViewController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CommentsViewController.h"
#import "CommentsView.h"
#import "CommentsModel.h"

#import "CommentTableViewCell.h"
#import "CommentsHeaderView.h"

#import <UIScrollView+SVPullToRefresh.h>

static NSString *const CommentsViewControllerCellReuseIdentifier = @"CommentsViewControllerCellReuseIdentifier";
static NSString *const CommentsViewControllerHeaderReuseIdentifier = @"CommentsViewControllerHeaderReuseIdentifier";

@interface CommentsViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) CommentsView *commentsView;
@property (nonatomic, strong) CommentsModel *model;

@end

@implementation CommentsViewController

- (instancetype)initWithPhoto:(Photo *)photo {
    self = [super init];
    if (self) {
        _model = [[CommentsModel alloc] initWithPhoto:photo];
    }
    return self;
}

- (void)loadView {
    self.commentsView = [[CommentsView alloc] init];
    self.view = self.commentsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.commentsView setPhoto:self.model.photo];
    
    self.commentsView.tableView.dataSource = self;
    self.commentsView.tableView.delegate = self;
    
    [self.commentsView.tableView registerClass:[CommentTableViewCell class] forCellReuseIdentifier:CommentsViewControllerCellReuseIdentifier];
    [self.commentsView.tableView registerClass:[CommentsHeaderView class] forHeaderFooterViewReuseIdentifier:CommentsViewControllerHeaderReuseIdentifier];

    __weak typeof(self) weakSelf = self;
    [self.model reloadDataWithCompletion:^{
        [weakSelf.commentsView setPhoto:weakSelf.model.photo];
        [weakSelf.commentsView.tableView reloadData];
    }];
    
    [self.commentsView.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf.model reloadDataWithCompletion:^{
            [weakSelf.commentsView setPhoto:weakSelf.model.photo];
            [weakSelf.commentsView.tableView reloadData];
            [weakSelf.commentsView.tableView.pullToRefreshView stopAnimating];
        }];
    } position:SVPullToRefreshPositionTop];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                          style:UIBarButtonItemStyleDone
                                                                         target:self action:@selector(back:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
}

#pragma mark - Actions

- (void)createComment:(id)sender {
    
    UIAlertController *createController = [UIAlertController alertControllerWithTitle:@"Add comment"
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [createController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Comment's text";
        
        UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        rightLabel.text = [NSString stringWithFormat:@"%i", 300];
        rightLabel.font = [UIFont systemFontOfSize:8.0f];
        rightLabel.textColor = [UIColor lightGrayColor];
        [rightLabel sizeToFit];
        
        textField.rightView = rightLabel;
        textField.delegate = self;
        textField.rightViewMode = UITextFieldViewModeAlways;
        
    }];
    
    UIAlertAction *createAction = [UIAlertAction actionWithTitle:@"Create"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             if (createController.textFields.firstObject.text.length > 0) {
                                                                 [self.model createItemWithText:createController.textFields.firstObject.text withCompletion:^(BOOL created) {
                                                                     if (created) {
                                                                         [self.commentsView.tableView reloadData];
                                                                     }
                                                                 }];
                                                             }
                                                         }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    
    [createController addAction:createAction];
    [createController addAction:cancelAction];
    [self presentViewController:createController animated:YES completion:nil];
}

- (void)back:(id)sender {
    [self.commentsView setUserInteractionEnabled:NO];
    [self.commentsView.tableView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    return [self.model numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CommentsViewControllerCellReuseIdentifier];
    [cell setItem:[self.model itemForRow:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIAlertController *deleteController = [UIAlertController alertControllerWithTitle:@"Delete comment"
                                                                              message:@"Are you sure?"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete"
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             [self.model deleteItemForRow:indexPath.row withCompletion:^(BOOL deleted) {
                                                                 if (deleted) {
                                                                     [tableView reloadData];
                                                                 }
                                                             }];
                                                         }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    
    [deleteController addAction:deleteAction];
    [deleteController addAction:cancelAction];
    [self presentViewController:deleteController animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [CommentTableViewCell heightForItem:[self.model itemForRow:indexPath.row] widht:tableView.bounds.size.width];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CommentsHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:CommentsViewControllerHeaderReuseIdentifier];
    headerView.textLabel.text = @"Comments:";
    [headerView.addButton addTarget:self action:@selector(createComment:) forControlEvents:UIControlEventTouchUpInside];
    return headerView;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger lenght = [textField.text stringByReplacingCharactersInRange:range withString:string].length;
    ((UILabel *)textField.rightView).text = [NSString stringWithFormat:@"%i", 300 - lenght];
    return lenght <= 300;
}

#pragma mark - UIScrollViewDelegate 

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
