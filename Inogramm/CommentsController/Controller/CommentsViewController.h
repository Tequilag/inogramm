//
//  CommentsViewController.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;

@interface CommentsViewController : UIViewController

- (instancetype)initWithPhoto:(Photo *)photo;

@end
