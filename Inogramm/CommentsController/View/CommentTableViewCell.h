//
//  CommentTableViewCell.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 19/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

+ (CGFloat)heightForItem:(id)item widht:(CGFloat)widht;

- (void)setItem:(id)item;

@end
