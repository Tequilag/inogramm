//
//  CommentsView.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CommentsView.h"
#import "PhotoContentView.h"

CGFloat const CommentsViewMargin = 8.0f;

@interface CommentsView ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) UIView *headerContainer;

@property (nonatomic, strong) PhotoContentView *photoContentView;

@end

@implementation CommentsView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setClipsToBounds:YES];
        
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect: [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
        _tableView.tableFooterView = [[UIView alloc] init];
        [self addSubview:_tableView];
        
        _contentInsets = UIEdgeInsetsMake(CommentsViewMargin,
                                          CommentsViewMargin,
                                          CommentsViewMargin,
                                          CommentsViewMargin);
        
        _headerContainer = [[UIView alloc] init];
        
        _photoContentView = [[PhotoContentView alloc] init];
        _photoContentView.layer.cornerRadius = 5.0f;
        _photoContentView.layer.backgroundColor = [UIColor hw_colorWithHexInt:INODeYorkColorHex].CGColor;
        [_headerContainer addSubview:_photoContentView];
        
        _tableView.tableHeaderView = _headerContainer;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.tableView.frame = self.bounds;
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    CGSize photoSize = [self.photoContentView sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    
    self.photoContentView.frame = CGRectMake(actualBounds.origin.x,
                                             actualBounds.origin.y,
                                             actualBounds.size.width,
                                             photoSize.height);
    
    self.headerContainer.frame = CGRectMake(0.0f,
                                            0.0f,
                                            self.bounds.size.width,
                                            actualBounds.origin.y + photoSize.height + 8.0f);
}

- (void)setPhoto:(Photo *)photo {
    [self.photoContentView setItem:photo];
}

- (void)setContainerBackground:(UIColor *)color {
    _photoContentView.layer.backgroundColor = color.CGColor;
}


@end
