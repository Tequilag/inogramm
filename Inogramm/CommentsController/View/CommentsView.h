//
//  CommentsView.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Photo;

@interface CommentsView : UIView

@property (nonatomic, strong) UITableView *tableView;

- (void)setContainerBackground:(UIColor *)color; //Animatable

- (void)setPhoto:(Photo *)photo;

@end
