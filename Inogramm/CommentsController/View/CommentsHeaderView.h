//
//  CommentsHeaderView.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 19/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) UIButton *addButton;

@end
