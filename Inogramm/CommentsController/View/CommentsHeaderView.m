//
//  CommentsHeaderView.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 19/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CommentsHeaderView.h"

@interface CommentsHeaderView ()

@property (nonatomic, assign) CGFloat backgroundCornerRadius;

@property (nonatomic, strong) UIView *backgroundContainer;
@property (nonatomic, strong) UIView *backgroundCornerContainer;
@property (nonatomic, strong) UIVisualEffectView *backgorundBlurView;

@end

@implementation CommentsHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _backgroundCornerRadius = 16.0f;
        
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        
        _backgroundContainer = [[UIView alloc] init];
        _backgroundContainer.backgroundColor = [UIColor clearColor];
        _backgroundContainer.clipsToBounds = YES;
        [self.backgroundView addSubview:_backgroundContainer];
        
        _backgroundCornerContainer = [[UIView alloc] init];
        _backgroundCornerContainer.clipsToBounds = YES;
        _backgroundCornerContainer.layer.cornerRadius = _backgroundCornerRadius;
        [_backgroundContainer addSubview:_backgroundCornerContainer];
        
        _backgorundBlurView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
        [_backgroundCornerContainer addSubview:_backgorundBlurView];
    
        _addButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        _addButton.tintColor = [UIColor hw_colorWithHexInt:INOJewelColorHex];
        [self.contentView addSubview:_addButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundView.frame = self.bounds;
    
    self.backgroundContainer.frame = CGRectMake(0.0f,
                                                0.0f,
                                                self.bounds.size.width,
                                                self.bounds.size.height - 1.0f);
    
    self.backgroundCornerContainer.frame = CGRectMake(0.0f,
                                                      0.0f,
                                                      self.bounds.size.width,
                                                      self.bounds.size.height + self.backgroundCornerRadius);
    
    self.backgorundBlurView.frame = self.backgroundCornerContainer.bounds;
    
    CGSize addButtonSize = [self.addButton sizeThatFits:self.bounds.size];
    self.addButton.frame = CGRectMake(self.bounds.size.width - self.backgroundCornerRadius * 2.0f - addButtonSize.width,
                                      0.0f,
                                      addButtonSize.width + self.backgroundCornerRadius * 2.0f,
                                      self.bounds.size.height);
}

@end
